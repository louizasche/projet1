



<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Blog etudiant</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" >
    <!-- Icon -->
    <link rel="stylesheet" href="assets/fonts/line-icons.css">
    <!-- Owl carousel -->
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/owl.theme.css">
    
    <link rel="stylesheet" href="assets/css/magnific-popup.css">
    <link rel="stylesheet" href="assets/css/nivo-lightbox.css">
    <!-- Animate -->
    <link rel="stylesheet" href="assets/css/animate.css">
    <!-- Main Style -->
    <link rel="stylesheet" href="assets/css/main.css">
    <!-- Responsive Style -->
    <link rel="stylesheet" href="assets/css/responsive.css">

  </head>
  <body>

    <!-- Header Area wrapper Starts -->
    <header id="header-wrap">
      <!-- Navbar Start -->
      <nav class="navbar navbar-expand-md bg-inverse fixed-top scrolling-navbar">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <a href="index.html" class="navbar-brand"><img src="assets/img/logo.png" alt=""></a>       
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <i class="lni-menu"></i>
          </button>
          <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto w-100 justify-content-end clearfix">
              <li class="nav-item active">
                <a class="nav-link" href="#hero-area">
                  ACCEUIL
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#services">
                  ÉCOLE
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#team">
                  COURS
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#pricing">
                  STAGES
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#testimonial">
                  BLOG
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#contact">
                  CONTACT
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- Navbar End -->

      <!-- Hero Area Start -->
      <div id="hero-area" class="hero-area-bg">
        <div class="container">      
          <div class="row">
            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
              <div class="contents">
                <h2 class="head-title">Bienvenue a ELITECH </h2>
                <h1>Laissez moi partager mon experience avec vous</h1>
                <p>Le Brevet du Technicien Supérieur Services informatique aux Organisations (SIO), s’adresse à ceux qui souhaitent se former en deux ans aux métiers d’administrateur réseau ou de développeur pour intégrer directement le marché du travail ou continuer des études dans le domaine de l’informatique et j'ai choisi Elitech pour concrétiser mes rêves. Il est temps pour moi de partager  mon experience avec vous.</p>
                <div class="header-button">
                  <a rel="nofollow" href="https://www.elitech.education/" class="btn btn-common" target="_blank" rel="noopener noreferrer">Le site officiel de l'école</a>
                  <a href="https://www.youtube.com/watch?v=nlaWQEL79N0" class="btn btn-border video-popup" target="_blank" rel="noopener noreferrer">Mon partenariat</a>
                </div>
              </div>
            </div>
            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
              <div class="intro-img">
                <img class="img-fluid" src="assets/img/sche.PNG" alt="">
              </div>            
            </div>
          </div> 
        </div> 
      </div>
      <!-- Hero Area End -->

    </header>
    <!-- Header Area wrapper End -->

    <!-- Services Section Start -->
    <section id="services" class="section-padding">
      <div class="container">
        <div class="section-header text-center">
          <h2 class="section-title wow fadeInDown" data-wow-delay="0.3s">ÉCOLE</h2>
          <p>Aprés la validation de ma premiere année j'ai passé un entretient a l'école pour un stage interne, j'ai eu l'immense honneur d'étre prise pour cette mission. J'ai travailé dans plusieurs services principalement pour l'assistance mais j'ai aussi pu participer a des projets  dans les autres services comme la partie administration,  commerciale, la partie admission et un peu de padagogie. Je vais vous faire part de ce que j'ai pu apprendre de l'école, let's go..</p>
          <div class="shape wow fadeInDown" data-wow-delay="0.3s"></div>
        </div>
        <div class="row">
          <!-- Services item -->
          <div class="col-md-6 col-lg-4 col-xs-12">
            <div class="services-item wow fadeInRight" data-wow-delay="0.3s">
              <div class="icon">
                <i class="lni-cog"></i>
              </div>
              <div class="services-content">
                <h3><a href="#">Le service admission</a></h3>
                <p>L'admission a l'école est un peu simple, comme toutes les autres écoles, un bac pour un BTS une deuxieme année validée pour une licence ou un bachelor et l'un de ces derniers pour un master. Aprés ça  depends des filliéres et des critéres d'admissions requises  pour chaque formation. Pour les etudiants etrangers comme moi la démarche ne diffère pas beaucoup si ce n'est quelques élements en plus pour valider le niveau de la langue et un payement pour la préinscription qui couvre le suivi du dossier.</p>
                <p>
                  05 Campus: Paris, Montpellier, Tunis, Londres et Elitech365
                  <p>De nombreuses filières: Commerce, Informatique, Rh, Santé, Economie et Droit.</p>
                  <p>Formations innovantes s'appuyant sur l'apprentissage, la pratique, l'alternance, des etudes et des experiences en entreprise et innovation technologique.</p>
                </p>
                <p>La demande d'admission se fait comme suite
                  <ul>
                    <li>Envoie des documents nécessaires</li>
                    <li>L'évaluation et l'étude des documents pour la filière demandé </li>
                    <li>La réponse du service d'admission et de la partie commerciale si le cursus se fait en alternance</li>
                    <li>Si la reponse est positive, la réception des indentifiants de connexion, du programme, calendrier, 
                      et tous les documents nécessaires a signer, quelques documents pour: ELITECH Autorisation captation image,
                      ELITECH CONTRAT D'ENGAGEMENT FINANCIER, ELITECH Reglement Interieur, ELITECH Service Santé Scolaire et 
                      une photo d’identité numérique pour la préparation de la carte d’étudiant. </li>
                    <li>Le paiement des frais d'inscriptions par chèques virement bancaire paypal ou espèce. </li>
                    <li>Finalisation de l'inscription</li>
                  </ul>
                </p>
                <p></p>
              </div>
            </div>
          </div>
          <!-- Services item -->
          <div class="col-md-6 col-lg-4 col-xs-12">
            <div class="services-item wow fadeInRight" data-wow-delay="0.6s">
              <div class="icon">
                <i class="lni-stats-up"></i>
              </div>
              <div class="services-content">
                <h3><a href="#">L'admission des etudiants étrangers </a></h3>
                <p> Le plus récurent c'est les candidatures via le site ou via l'adresse mail du service admission. Ensuite viens l'envoi des documents de candidature : CV, lettre de motivation, pièce d'identité, dernier diplôme obtenu,
                 relevet de notes des deux dernières années, test de français (TCF valide, DELF ou DALF) pour les étudiants ressortissant des pays hors UE et le choix de formation  puis l'étude du dossier par l'équipe d'admission
                  et l'entretien qui sera  téléphonique ou via Teams. Si l'étudiant est apte a intégrer la formation demandé il va recevoir un mail d'acceptation indiquant les modalités de paiement et les détails relatifs à la formation choisie,
                  le paiement des frais de préinscription (400€) donnant droit à une attestation de préinscription ou en plus d'un acompte sur les frais de scolarité : donnant droit à une attestation d'inscription  que l'etudiant va recevoir + l'attestation de paiement.
                  En parallèle se fait la création d'un dossier Campus France et téléchargement de l'attestation de l'école. Soumission du dossier Campus France, le paiement des frais et prise de RDV pour l'entretien de motivation.
                  Ensuite vient l'impression de "l'accord préalable d'inscription" à partir de la plateforme Campus France et sa signature suite à l'entretien. Et enfin la prise de RDV auprès du consulat de France 
                  pour la constitution et dépôt d'un dossier de demande de visa long séjour d'études, incluant l'accord préalable d'inscription, les documents relatifs à l'hébergement et aux moyens financiers de subsistance en France. De la on attend le  retour du statut visa : accordé ou refusé  </p>
              </div>
            </div>
          </div>
          <!-- Services item -->
          <div class="col-md-6 col-lg-4 col-xs-12">
            <div class="services-item wow fadeInRight" data-wow-delay="0.9s">
              <div class="icon">
                <i class="lni-users"></i>
              </div>
              <div class="services-content">
                <h3><a href="#">Le service assistance</a></h3>
                <p>Le service assistance dans une école est indispensable, surtout en cette période de crise sanitaire ou la pluparts des cours, des réunions et des examens se font en ligne.
                  Sa fonction globale est de gérer tout l'informatique. C'est-à-dire qu'ils installent le parc informatique, et les nouveaux éléments quand ils arrivent, et ils s'occupent de la maintenance. 
                  quand un problème informatique survient, c'est eux qui interviennent. 
                  <ul>
                    <li>Assurer la maintenance informatique préventive et curative.</li>
                    <li>Optimiser le système informatique des employés en fonction de l’usage et des besoins de l’ecole.</li>
                    <li>Améliorer la sécurité informatique de l'école </li>
                    <li>Proposer des axes d’améliorations du parc informatique.</li>
                    <li>Dépanner, remettre en état en cas de besoin</li>
                    <li></li>
                  </ul>
                <p>Ce service est aussi en contact direct avec les étudiants et les professeurs pour s'assurer du bon déroulement des cours, des devoirs et des examens pour réagir rapidement en cas d’incident informatique.
                  Assister les utilisateurs, voire les guider dans l’utilisation de leurs appareils. S'assurer du lancement des cours en ligne du bon fonctionnement pour toutes les parties et de l'enregistrement de celui-ci.  </p>
                <p>C’est en travaillant sur cette partie que j’ai eu comme travail de modifier les dates du site et de le mettre à jour à chaque évènements. à la demande de mon responsable j'ai créé ce blog 
                  pour parler de mon expérience en tant qu’étudiante de l'école et interne pour mon stage. </p>
                <p>Le service assistance est celui qui touche le plus à  la partie informatique dans une école.</p>
              </div>
            </div>
          </div>
          <!-- Services item -->
          <div class="col-md-6 col-lg-4 col-xs-12">
            <div class="services-item wow fadeInRight" data-wow-delay="1.2s">
              <div class="icon">
                <i class="lni-layers"></i>
              </div>
              <div class="services-content">
                <h3><a href="#">LE SERVICE Administration</a></h3>
                <p>Ce service supervise les tâches administratives dans l’écoles. Les membres de l’équipe veillent au bon fonctionnement de l'organisation 
                  et gèrent également les installations et le personnel et veillent à ce que l’école se conforme aux lois et aux réglementations en vigueur.
                  Ils s'occupent de Coordonner tous les processus administratifs, Développer et gérer des programmes éducatifs, Embaucher, former et conseiller le personnel
                  et les étudiants en cas de besoin, communiquer avec les parents, les organismes de réglementations et le public, aider à façonner et à promouvoir la vision de l’école
                  gérer les budgets, les politiques, les apporteur d'affaires et les évènements. <br>
                  Tout comme repose sur eux  la résolution des conflits ou d'autres problèmes au fur et à mesure qu'ils se produisent. Ils s'assurent  de faire fonctionner tous les programmes. <br>
                  Ce travail est effectuer dans tous les campus ELITECH et toutes les equipes administratives communiquent quotidiennement entre elles via teams.   
                </p>
              </div>
            </div>
          </div>
          <!-- Services item -->
          <div class="col-md-6 col-lg-4 col-xs-12">
            <div class="services-item wow fadeInRight" data-wow-delay="1.5s">
              <div class="icon">
                <i class="lni-mobile"></i>
              </div>
              <div class="services-content">
                <h3><a href="#">LE SERVICE commerciale</a></h3>
                <p>ELITECH s’attache à développer des synergies avec les entreprises, dans une démarche de relation durable. Les alternances permettent aux étudiants de développer leurs compétences, ainsi que leur savoir-faire.
                  L'équipe commerciale prospectent des etudiants et des entreprises, elle favorise le cursus en alternance, elle place les etudiants en entreprise pour des contrats d'apprentissage, de professionnalisation ou des stages.
                  ce qui permet de concevoir un projet professionnel complet grâce à une formation qualifiante et une expérience concrète en entreprise. 
                  Les avantages de l’alternance sont les suivants :
                <ul>
                  <li>Obtenir un diplôme ou une qualification parmi un large choix de métiers </li>
                  <li>Bénéficier de la gratuité des frais de formation. </li>
                  <li>Mettre en pratique les enseignements. </li>
                  <li>Être rémunéré, pendant sa formation. </li>
                  <li>Accéder plus facilement à l’emploi, grâce à l’expérience professionnelle acquise en entreprise. </li>
                </ul>
                </p>
              </div>
            </div>
          </div>
          <!-- Services item -->
          <div class="col-md-6 col-lg-4 col-xs-12">
            <div class="services-item wow fadeInRight" data-wow-delay="1.8s">
              <div class="icon">
                <i class="lni-rocket"></i>
              </div>
              <div class="services-content">
                <h3><a href="#">LE SERVICE PÉDAGOGIE</a></h3>
                <p>La mission de l’Équipe Pédagogique est d'améliorer l’apprentissage des étudiants, en travaillant sur les modes d'enseignements, les stages et les évaluations. Les objectifs de ce module sont : 
                  <ul>
                    <li>La dématérialisation du cahier de texte</li>
                    <li>L'évaluation des apprenants sur les activités pédagogiques au centre et/ou en entreprise</li>
                    <li>Le Suivi et La  gestion des dossiers administratifs et pédagogique</li>
                    <li>L’échange des documents entre les 3 acteurs (formateur, apprenant, entreprise)</li>               
                  </ul> 
                 </p>
                 <p> Nous référençons  la liste de nos diplômes  parceque chaque diplôme à son plan d'étude (avec nombre d’heures, et nombre de cours à donner). Déterminer les calendriers de formations par rapport aux nombres d'heures minimums par diplôme.
                  Nous suivons aussi le niveau de consommation des heures de cours par rapport au plan d'étude, sur un calendrier <br>
                  Le service gere aussi les  listes d'enseignants et d'étudiants, la gestion des examens et des notes etc..
                  </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Services Section End -->

    <!-- About Section start -->
    <section id="team" class="section-padding">

    <div class="about-area section-padding bg-gray">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-md-12 col-xs-12 info">
            <div class="about-wrapper wow fadeInLeft" data-wow-delay="0.3s">
              <div>
                <div class="site-heading">
                  <p class="mb-3"></p>
                  <h2 class="section-title">LA VIE EN COURS</h2>
                </div>
                <div class="content">
                  <p>
                    Pour ma part je suis sur campus Paris, niveau emplacement c'est trés bien il y a une facilité d'accès -Paris centre - Châtlet/Les Halles. Les locaux sont adaptés,
                    convivial et spacieux. Les équipements sont de haut de gamme. Et les cours sont au choix, presentiel ou en ligne avec un nombre de places limitées vu les conditions actuelles.
                    Les formations diplômantes BTS -Bachelors -Masters en adéquation avec les differents niveaux de collaboration, ces dernières sont aux aspects «savoir être» et intégration en entreprise .
                    Les enseignements sont assurés par des professionnels expérimentés, prenant en compte les réalités du monde du travail et de la vie en entreprise. 
                    Il y a un accompagnement et un suivi des étudiants durant toute la période d'alternance. Un interlocuteur unique Elitech est dédiée à votre entreprise pour le suivi de votre ou vos coloborateurs -altrnants.
                     Sans oublier que nos calendriers sont en cohérence avec notre activité en entreprise selon la spécialisations. Ci dessous les differentes perspectives.
                  </p>
                  <a href="https://www.elitech.education/specialisations/" class="btn btn-common mt-3" target="_blank" rel="noopener noreferrer">SPÉCIALISATIONS</a>
                   <hr>
                  <hr>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-md-12 col-xs-12 wow fadeInRight" data-wow-delay="0.3s">
            <img class="img-fluid" src="assets/img/about/img-1.png" alt="" >
          </div>
          <div class="col-lg-12 col-md-12 col-xs-12">
            <div>
              
                <div class="site-heading">
                  <h2 class="section-title">Plus de details sur le BTS SIO ?</h2>

                </div>
                <div class="content">
          <p>BTS SIO – Système d’Information et Organisation autrement nommé le Brevet de Technicien Supérieur Services informatique aux Organisations (SIO) 
            s’adresse à ceux qui souhaitent se former en deux ans aux métiers d’administrateur réseau ou de développeur pour intégrer directement le marché du travail
            ou continuer des études dans le domaine de l’informatique.</p>
            <p>Qu’est-ce que le BTS SIO ?</p>
            <p>Le BTS Services informatiques aux Organisations est un diplôme reconnu par l’état de niveau Bac+2.
              Il remplace depuis septembre 2011 l’ancien BTS informatique de Gestion (BTS IG). 
              En effet, cette nouvelle formule répond aux attentes de la profession en matière de qualification. 
              Le programme a évolué en fonction de la place et du rôle des TIC (Technologies d’Information Communication)
               et propose deux spécialités bien distinctes : le BTS SIO SISR et le BTS SIO SLAM.
            <br>Il associe aux enseignements théoriques et professionnels (en contrôle continu et final) deux stages en entreprise (10 à 11 semaines réparties sur les deux années d’études).
            <br>En plus des matières en lien avec l’ingénierie informatique, la formation propose également des enseignements de culture générale, d’anglais (avec la possibilité de continuer une LV2)
               de mathématiques appliquées à l’informatique, ainsi que sur les outils d’analyse économique, managériale et juridique des services informatiques.
            </p>
            <p>Les options du BTS SIO :</p>
            <p>Le BTS SIO option SISR : L’option Solutions d’infrastructure, systèmes et réseaux forme des professionnels des réseaux et équipements informatiques (installation, maintenance, sécurité). En sortant d’un BTS SIO SISR, vous serez capable de gérer et d’administrer le réseau d’une société et d’assurer sa sécurité et sa maintenance.
              Les techniciens supérieurs en informatique option SISR, peuvent accéder aux métiers de :</p>
              <ul>
                <li>– Administrateur systèmes et réseaux</li>
                <li>– Informaticien support et déploiement</li>
                <li>– Pilote d’exploitation</li>
                <li>– Support systèmes et réseaux</li>
                <li>– Technicien d’infrastructure</li>
                <li>– Technicien de production</li>
                <li>– Technicien micro et réseaux</li>
              </ul>
            <p>Le BTS SIO option SLAM : L’option Solutions logicielles et applications métiers forment des spécialistes des logiciels (rédaction d’un cahier des charges, formulation des besoins et spécifications, développement, intégration au sein de la société). Les techniciens supérieurs en informatique option slam, sont préparés aux métiers de :</p>
              <li>– Développeur d’applications informatiques</li>
              <li>– Développeur informatique</li>
              <li>– Analyste d’applications ou d’études</li>
              <li>– Analyste programmeur</li>
              <li>– Chargé d’études informatiques</li>
              <li>– Informaticien d’études</li>
              <li>– Programmeur analyste</li>
              <li>– Programmeur d’applications</li>
              <li>– Responsable des services applicatifs</li>
              <li>– Technicien d’études informatiques</li>
            </ul>
            <p>Le BTS SIO – Services informatiques aux organisations se prépare en deux ans après un bac STMG ou un bac pro SEN.</p>
            <p>C’est un diplôme de niveau bac +2 qui se prépare en formation initiale mais aussi en alternance. </p>
            <p>Le titulaire du diplôme participe à la production et à la fourniture de services en réalisant ou en adaptant des solutions d’infrastructure et en assurant le fonctionnement optimal des équipements.</p>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </section>
    <!-- About Section End -->

    <!-- Features Section Start -->
    <section id="features" class="section-padding">
      <div class="container">
        <div class="section-header text-center">          
          <h2 class="section-title wow fadeInDown" data-wow-delay="0.3s" style="color: black;">LES BTS</h2>
          <p>Il existe des BTS dans tous les domaines. Du commercial à la chimie, de la mécanique au textile, du tourisme au bâtiment, 
            en passant par la publicité… les BTS couvrent tous les secteurs d’activité. Les BTS agricoles sont appelés « BTSA » pour Brevet de Technicien Supérieur Agricole). 
            Plus de la moitié des spécialités de BTS sont représentées dans le Nord-Pas de Calais et la Picardie. Cependant,
             certaines spécialités comptent des milliers de candidats alors que d’autres n’en comptent seulement qu’une centaine. 
             À vous de faire le bon choix pour mettre toutes les chances de votre côté ! Voici  la liste de BTS que notre école propose </p>
          <div class="shape wow fadeInDown" data-wow-delay="0.3s"></div>
        </div>
        <div class="row">
          <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
            <div class="content-left">
              <div class="box-item wow fadeInLeft" data-wow-delay="0.3s">
                <span class="icon">
                  <i class="lni-rocket"></i>
                </span>
                <div class="text">
                  <h4 style="color: black;">BTS SIO</h4>
                  <p style="color: black;">(BAC + 2) Services informatiques aux organisations - options: SLAM/SISR. 
                  </p>
                </div>
              </div>
              <div class="box-item wow fadeInLeft" data-wow-delay="0.6s">
                <span class="icon">
                  <i class="lni-laptop-phone"></i>
                </span>
                <div class="text">
                  <h4 style="color: black;">BTS MCO</h4>
                  <p style="color: black;">(BAC + 2) MCO: Un acronyme Management commercial opérationnel (EX-BTS MUC) </p>
                </div>
              </div>
              <div class="box-item wow fadeInLeft" data-wow-delay="0.9s">
                <span class="icon">
                  <i class="lni-cog"></i>
                </span>
                <div class="text">
                  <h4 style="color: black;">BTS NDRC</h4>
                  <p style="color: black;">(BAC + 2) Négociation et digitalisation de la relation client (EX-BTS NRC)</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
            <div class="show-box wow fadeInUp" data-wow-delay="0.3s">
              <img src="assets/img/feature/intro-mobile.png" alt="" height="450">
            </div>
          </div>
          <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
            <div class="content-right">
              <div class="box-item wow fadeInRight" data-wow-delay="0.3s">
                <span class="icon">
                  <i class="lni-leaf"></i>
                </span>
                <div class="text">
                  <h4 style="color: black;">BTS COMMERCE INTERNATIONAL</h4>
                  <p style="color: black;">(BAC + 2) Préparant aux métiers du commerce International</p>
                </div>
              </div>
              <div class="box-item wow fadeInRight" data-wow-delay="0.6s">
                <span class="icon">
                  <i class="lni-layers"></i>
                </span>
                <div class="text">
                  <h4 style="color: black;">BTS NO - NOTARIAT</h4>
                  <p style="color: black;">(BAC + 2) Etude des matières générales du droit et des matières spécifiques au domaine notarial</p>
                </div>
              </div>
              <div class="box-item wow fadeInRight" data-wow-delay="0.9s">
                <span class="icon">
                  <i class="lni-leaf"></i>
                </span>
                <div class="text">
                  <h4 style="color: black;">BTS CRÉATEUR DIGITAL</h4>
                  <p style="color: black;">(BAC + 2) Etude des aspects de la conception à la création de la charte graphique à l'identité digitale </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Features Section End -->   

    <!-- Call To Action Section Start -->
    <section id="cta" class="section-padding">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-xs-12 wow fadeInLeft" data-wow-delay="0.3s">           
            <div class="cta-text">
              <h4>CURSUS FRANÇAIS</h4>
              <p>
                <ul>
                  <li>- Pour un BTS MCO, BTS SIO, BTS NDRC.. etc. (BAC + 2)</li>
                  <li>- Responsable Commercial & Marketing (Marketing Digital), Concepteur Developpeur d'Applications, Administrateur/trice d'Infrastructures Sécurisées...
                    TITRE RNCP 31113 NIVEAU 6 – (BAC + 3) – Titre RNCP niveau 6 </li> 
                  <li>- Ingénieur d’Affaire, Manager De Projets Informatique, Management Des Ressources Humaines – (BAC + 5) – Titre RNCP Niveau 7</li>
                </ul> - 
                 
              </p>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-xs-12 text-right wow fadeInRight" data-wow-delay="0.3s">
            </br><a rel="nofollow" href="https://www.elitech.education/admission/" class="btn btn-common" target="_blank" rel="noopener noreferrer">FORMULAIRE</a>
          </div>
        </div>
      </div>
    </section>
    <section id="cta" class="section-padding">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-xs-12 wow fadeInLeft" data-wow-delay="0.3s">           
            <div class="cta-text">
              <h4>CURSUS ANGLAIS</h4>
              <p>English program - Registration form</p> 
              <p>L3 International Foundation Diploma
                International Baccalaureate in English</p>
              <p>At ELITECH International University, we create English degree courses, which, both academically and personally, prepare you for the working world. </p>
              <p>The Level 3 International Foundation Diploma for Higher Education Studies (L3IFDHES) is a one-year pre-university qualification which includes a significant degree of English language teaching to equip you with the skills and expertise to further your studies and progress onto Level 4 (Bac+1) Level 5 (Bac +2) Level 6 (Bac +3) Diplomas in Computer Science and Business Management.</p>
              <p>Your programme of Study:
                <ul>
                  <li>level 4 (Bac+1) Computing with Business Management</li>
                  <li>Level 4 (Bac+1) Business Management,</li>
                  <li>Level 5 (Bac +2) Computing</li>
                  <li>Level 5 (Bac+2) Computing with Cyber Security</li>
                  <li>Level 5 (Bac+3) Business Management</li>
                  <li>Level 6(Bac +3) Computing and Information Systems</li>
                  <li>Level 6 (Bac+3) Business Strategy and Business Management.</li>
                </ul>
              </p>
              

            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-xs-12 text-right wow fadeInRight" data-wow-delay="0.3s">
            </br><a rel="nofollow" href="https://www.elitech.education/admissionuk/" class="btn btn-common" target="_blank" rel="noopener noreferrer">FORMULAIRE</a>
          </div>
        </div>
      </div>
    </section>
    <!-- Call To Action Section Start -->

    <!-- Pricing section Start --> 
    <section id="pricing" class="section-padding">
      <div class="container">
        <div class="section-header text-center">          
          <h2 class="section-title wow fadeInDown" data-wow-delay="0.3s">STAGE</h2>
          <p>Le Brevet de Technicien Supérieur (BTS) Services Informatiques aux Organisations (SIO) est un diplôme qui se prépare en 2 ans après le bac.
            Plusieurs périodes de stage complètent la formation. Pour ma part mon stage est en interne a l'école. Mais pour les autres etudiants l'équipe commerciale se charge de trouver des stages pour permettre aux etudiants 
            de valider leurs années. <br>
            Des cours pour la rédaction  du CV et la préparation pour le monde de l'entreprise se font tout au long de l'année pour avoir le plus de chances de trouver le stage parfait:
          </p>
          <div class="shape wow fadeInDown" data-wow-delay="0.3s"></div>
        </div>
        <div class="row">
          <div class="col-lg-4 col-md-6 col-xs-12">
            <div class="table wow fadeInLeft" data-wow-delay="1.2s">
              <div class="icon-box">
                <i class="lni-package"></i>
              </div>
              <div class="pricing-header">
                <p class="price-value">La rédaction du CV<span> <br>
                  De plus au séances de PMR, les commerciaux en charge de chaque etudiants les aident pour la demarche.</span></p>
              </div>
              <div class="title">
                <h3>Rediger son cv sur un site en ligne</h3>
              </div>
              <ul class="description">
                <li> C'EST SIMPLE, C'EST RAPIDE ET C'EST GRATUIT !</li>
                <li>Mettre en evidences les instructions du commercial</li>
                <li></li>
                <li></li>
              </ul>
              <button class="btn btn-common"><a style="color: white;" href="https://cvdesignr.com/fr" target="_blank" rel="noopener noreferrer">Cv designr<i class="pi pi-question-circle"></i></a></button>

            </div> 
          </div>
          <div class="col-lg-4 col-md-6 col-xs-12 active">
            <div class="table wow fadeInUp" id="active-tb" data-wow-delay="1.2s">
              <div class="icon-box">
                <i class="lni-drop"></i>
              </div>
              <div class="pricing-header">
                <p class="price-value">Candidater<span> <br>
                  Postuler sur des sites et des plateformes et essayer d'entrer en contact avec les recruteurs </span></p>
              </div>
              <div class="title">
                <h3>Trouvez le stage Adapté à votre formation</h3>
              </div>
              <ul class="description">
                <li>Stage.fr, Kapstages, Jd.apec.fr, Studyrama-emploi, Monstageenligne, LinkedIn, Indeed...</li>
                <li></li>
                <li></li>
                <li></li>
              </ul>
              <button class="btn btn-common"><a style="color: white;" href="https://fr.indeed.com/" target="_blank" rel="noopener noreferrer">Indeed <i class="pi pi-question-circle"></i></a></button>
           </div> 
          </div>
          <div class="col-lg-4 col-md-6 col-xs-12">
            <div class="table wow fadeInRight" data-wow-delay="1.2s">
              <div class="icon-box">
                <i class="lni-star"></i>
              </div>
              <div class="pricing-header">
                <p class="price-value">Préparer son entretien<span> <br>
                  Votre CV a eu du succès et vous voilà convoqué pour un entretien qui débouchera  je l’espère  sur un  stage !</span></p>
              </div>
              <div class="title">
                <h3>Comment réussir un entretien de stage ?</h3>
              </div>
              <ul class="description">
                <li>Préparer son discours et se renseigner sur l’entreprise, le poste et ce qu’on attendra de vous</li>
                <li></li>
                <li></li>
                <li></li>
              </ul>
              <button class="btn btn-common"><a style="color: white;" href="https://www.monster.fr/conseil-carriere/article/stage-entretien" target="_blank" rel="noopener noreferrer">Préparation <i class="pi pi-question-circle"></i></a></button>
            </div> 
          </div>
        </div>
      </div>
    </section>
    <!-- Pricing Table Section End -->
  
    <!-- Testimonial Section Start -->
    <section id="testimonial" class="testimonial section-padding">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="color: black;">
            <h1>Winners</h1>
            <p> 
              À l’école et plus précisément dans le service commercial il y a une équipe qu'on nomme les Winners qui se charge de trouver des postes en alternances aux étudiants déjà inscrits
              à l’école et recrutent aussi  des personnes adaptées aux offres des entreprises partenaires. cette dernière accompagne l’étudiant dans ses démarches,
               de la préparation du CV a la signature du contrat. <br>
               Dans un environnement en perpétuelle évolution technologique et méthodique, le directeur commercial et ses commerciaux doivent se former régulièrement sur les nouvelles méthodes de travail,
                les outils technologiques pour augmenter le chiffre d'affaire et acompagner au mieux les etudiants. <br>
                Une pratique intéressante, adoptée par certaines  écoles et entreprises, est la mise à disposition en interne d’un formateur ou d’outils e-learning 
                mettant à niveau l’équipe commerciale sur les nouvelles méthodes et outils ce qui est le cas d'ELITECH    </p>
                Le fonctionnement de l'équipe : <br>
                <ol>
                  <li>- Composer son équipe </li>
                  <li>- Établir un plan d’action </li>
                  <li>- Former en continu </li>
                  <li>- Accompagner pour consolider la relation</li>
                  <li>- Contrôler et suivre l’activité commerciale </li>
                  <li>- Bien s’équiper</li>
                  <li>- Motiver pour renforcer l’adhésion de l’équipe </li> <br>
                </ol> <br>


            <div id="testimonials" class="owl-carousel wow fadeInUp" data-wow-delay="1.2s">
              
              <div class="item">
                <div class="testimonial-item">
                  <div class="img-thumb">
                    <img src="assets/img/testimonial/img1.jpg" alt="">
                  </div>
                  <div class="info">
                    <h2><a href="https://www.linkedin.com/in/mahila-benia-60940b163/" target="_blank" rel="noopener noreferrer">Mahila BENIA</a></h2>
                    <h3><a href="https://www.linkedin.com/in/mahila-benia-60940b163/" target="_blank" rel="noopener noreferrer">Business developer</a></h3>
                  </div>
                  <div class="content">
                    <p class="description"> 
                       Participe activement au développement du chiffre d’affaires d’Elitech. De la prospection, à la négociation à la conclusion des contrats. Détecte les nouvelles opportunités, Attire de nouveaux clients De ce fait, Elle deploie une stratégie préalablement définie avec les différents services avec lesquels elle travaille, ce qui la différencie entre autre du commercial classique.
                    </p>
                    
                    
                    <div class="star-icon mt-3">
                      <span><i class="lni-star-filled"></i></span>
                      <span><i class="lni-star-filled"></i></span>
                      <span><i class="lni-star-filled"></i></span>
                      <span><i class="lni-star-filled"></i></span>
                      <span><i class="lni-star-half"></i></span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item">
                  <div class="img-thumb">
                    <img src="assets/img/testimonial/img2.jpg" alt="">
                  </div>
                  <div class="info">
                    <h2><a href="https://www.linkedin.com/in/fatima-leguemiri-527220b5/" target="_blank" rel="noopener noreferrer">Fatima LEGUEMIRI </a></h2>
                    <h3><a href="https://www.linkedin.com/in/fatima-leguemiri-527220b5/" target="_blank" rel="noopener noreferrer">Ingénieur d'affaires</a></h3>
                  </div>
                  <div class="content">
                    <p class="description"> Très bonne négociatrice. Cela passe par sa communication verbale et non verbale. L’organisation et la rigueur sont ses pièces maîtresses. Bien sur, le management de différentes équipes lui impose d’être un bon manager.
                      Chargée de suivre des projets à fortes valeurs ajoutées du début à la fin. Participe au devloppement du chiffre d'affaires d'Elitech.</p>
                    <div class="star-icon mt-3">
                      <span><i class="lni-star-filled"></i></span>
                      <span><i class="lni-star-filled"></i></span>
                      <span><i class="lni-star-filled"></i></span>
                      <span><i class="lni-star-half"></i></span>
                      <span><i class="lni-star-half"></i></span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item">
                  <div class="img-thumb">
                    <img src="assets/img/testimonial/img3.jpg" alt="">
                  </div>
                  <div class="info">
                    <h2><a href="https://www.linkedin.com/in/rafik-selmi-810744194/" target="_blank" rel="noopener noreferrer">Rafik SELMI</a></h2>
                    <h3><a href="https://www.linkedin.com/in/rafik-selmi-810744194/" target="_blank" rel="noopener noreferrer">Commercial</a></h3>
                  </div>
                  <div class="content">
                    <p class="description">Gestion du réseau professionnel au sein de l’école et en dehors, Gestion des clients,
                      création de fiches et de classements selon des degrés d’importance, de potentiel et de management des équipes 
                      selon la taille de l’entreprise et les outils disponible.
                      La prospection, La vente, La gestion administrative et commerciale adaptée à l’entreprise.. </p>
                    <div class="star-icon mt-3">
                      <span><i class="lni-star-filled"></i></span>
                      <span><i class="lni-star-filled"></i></span>
                      <span><i class="lni-star-filled"></i></span>
                      <span><i class="lni-star-filled"></i></span>
                      <span><i class="lni-star-half"></i></span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item">
                  <div class="img-thumb">
                    <img src="assets/img/testimonial/img4.jpg" alt="">
                  </div>
                  <div class="info">
                    <h2><a href="https://www.linkedin.com/in/rihab-amri-5b47b1123/" target="_blank" rel="noopener noreferrer">Rihab AMRI</a></h2>
                    <h3><a href="https://www.linkedin.com/in/rihab-amri-5b47b1123" target="_blank" rel="noopener noreferrer">Directrice commerciale</a></h3>
                  </div>
                  <div class="content">
                    <p class="description">Responsable des activités de prospection et de relations clients. En charge du développement du chiffre d'affaires et de la politique commerciale de l'école, 
                      Elle supervise l'équipe commerciale (en direct ou par le biais du réseau teams). Rattaché à la direction générale ou au chef d’entreprise (Le directeur de l'école).
                      Elle élabore la stratégie et les plans d'actions.</p>
                    <div class="star-icon mt-3">
                      <span><i class="lni-star-filled"></i></span>
                      <span><i class="lni-star-filled"></i></span>
                      <span><i class="lni-star-filled"></i></span>
                      <span><i class="lni-star-filled"></i></span>
                      <span><i class="lni-star-filled"></i></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Testimonial Section End -->

    <!-- Call To Action Section Start -->
    <section id="cta" class="section-padding">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-xs-12 wow fadeInLeft" data-wow-delay="0.3s">           
            <div class="cta-text">
              <h4>POURQUOI CHOISIR ELITECH ? </h4>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-xs-12 text-right wow fadeInRight" data-wow-delay="0.3s">
            </br><a href="https://www.elitech.education/pourquoi-choisir-elitech/" target="_blank" rel="noopener noreferrer" class="btn btn-common">En savoir plus</a>
          </div>
        </div>
      </div>
    </section>
    <!-- Call To Action Section Start -->

    <!-- Contact Section Start -->
    <section id="contact" class="section-padding bg-gray">    
      <div class="container">
        <div class="section-header text-center">          
          <h2 class="section-title wow fadeInDown" data-wow-delay="0.3s">CONTACT</h2>
          <div class="shape wow fadeInDown" data-wow-delay="0.3s"></div>

          <form method='POST'>
          <div>    <input type="email" name="Email" placeholder="Entrez votre email"></input></div>
            
          <div>   <input type="text" name="text" placeholder="Entrez votre message"></input></div>
            
          <div>  <button type="submit" id="submit">Valider</button></div>
          </form>

          <?php
		if (isset($_POST['prenom'])) {
			$mysqli = new mysqli("localhost", "root", "", "essai");
            $mysqli -> set_charset("utf8");
            $requete="INSERT INTO carnet VALUES(NULL, "' . $_POST['email'] . '", "' . $_POST['message'] . '")';
            $resultat = $mysqli -> query($requete);
            if ($resultat)
                echo "<p>Le message a été envoyé</p>";
            else
                echo "<p>Erreur</p>";
		  }
		  ?>
        </div>
        <div class="row contact-form-area wow fadeInUp" data-wow-delay="0.3s">   
          <div class="col-lg-7 col-md-12 col-sm-12">
    <!-- Call To Action Section Start -->
    <section id="cta" class="section-padding">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-xs-12 wow fadeInLeft" data-wow-delay="0.3s">           
            <div class="cta-text" style="color: black;">
              <h4>CAMPUS PARIS</h4> <br>
              <br>

              <h5>15 rue du Louvre 75001 Paris, France </h5>
              <h5>Tél : +33 1 88 88 00 50 </h5>
              <h5>WhatsApp : ‭+ 33 6 34 47 56 13 </h5>
              <h5>Email : paris@elitech.education </h5>
              

              
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-xs-12 text-right wow fadeInRight" data-wow-delay="0.3s">
            </br><a rel="nofollow" href="https://www.elitech.education/campus/" target="_blank" rel="noopener noreferrer" class="btn btn-common">LES CAMPUS</a>
          </div>
        </div>
      </div>
    </section>
    <!-- Call To Action Section Start -->
          </div>
          <div class="col-lg-5 col-md-12 col-xs-12">
            <div class="map">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2624.721976848998!2d2.3398203156745625!3d48.863511779288!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e66e23aa88e3c9%3A0x25dcbca1e511937b!2s15%20Rue%20du%20Louvre%2C%2075001%20Paris!5e0!3m2!1sen!2sfr!4v1613947572616!5m2!1sen!2sfr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>
          </div>
        </div>
      </div> 
    </section>
    <!-- Contact Section End -->

    <!-- Footer Section Start -->
    <footer id="footer" class="footer-area section-padding">
      <div class="container">
        <div class="container">
          <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
              <div class="widget">
                <h3 class="footer-logo"><img src="" alt=""></h3>
                <div class="textwidget">
                  <p>L’école Elitech, fondée en 1991.</p>
                </div>
                <div class="social-icon">
                  <a class="facebook" href="https://www.facebook.com/elitech.education" target="_blank" rel="noopener noreferrer"><i class="lni-facebook-filled"></i></a>
                  <a class="twitter" href="https://twitter.com/elitechedu?lang=en" target="_blank" rel="noopener noreferrer"><i class="lni-twitter-filled"></i></a>
                  <a class="instagram" href="https://www.instagram.com/elitech.education/" target="_blank" rel="noopener noreferrer"><i class="lni-instagram-filled"></i></a>
                  <a class="linkedin" href="https://www.linkedin.com/school/elitecheducation/" target="_blank" rel="noopener noreferrer"><i class="lni-linkedin-filled"></i></a>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
              <h3 class="footer-titel">ETUDIANTS</h3>
              <ul class="footer-link">
                <li><a href="https://www.elitech.education/alternance-etudiant/" target="_blank" rel="noopener noreferrer">ALTERNANCE </a></li>
                <li><a href="https://www.elitech.education/financement-des-etudes/" target="_blank" rel="noopener noreferrer">FINANCEMENT DES ÉTUDES </a></li>
                <li><a href="https://www.elitech.education/debouches-et-creation-dentreprise/" target="_blank" rel="noopener noreferrer">DÉBOUCHÉS ET CRÉATION D'ENTREPRISE </a></li>         
              </ul>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
              <h3 class="footer-titel">ENTREPRISES</h3>
              <ul class="footer-link">
                <li><a href="https://www.elitech.education/alternance-entreprise/" target="_blank" rel="noopener noreferrer">ALTERNANCE </a></li>
                <li><a href="https://www.elitech.education/relations-entreprises/" target="_blank" rel="noopener noreferrer">JOB FORUM</a></li>
                <li><a href="https://www.elitech.education/participez-au-developpement-de-elitech/" target="_blank" rel="noopener noreferrer">PARTICIPEZ AU DÉVELOPPEMENT DE L’ECOLE ELITECH </a></li>
              </ul>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
              <h3 class="footer-titel">CONTACT</h3>
              <ul class="address">
                <li>
                  <a href="https://www.elitech.education/campus/" target="_blank" rel="noopener noreferrer"><i class="lni-map-marker"></i> LOCALISATION 
                </li>
                <li>
                  <a href="https://www.elitech.education/contact-2/" target="_blank" rel="noopener noreferrer"><i class="lni-phone-handset"></i> CONTACTEZ L’ÉCOLE </a>
                </li>
                <li>
                  <a href="#"><i class="lni-envelope"></i> E: contact@elitech.education</a>
                </li>
              </ul>
            </div>
          </div>
        </div>  
      </div> 
      <div id="copyright">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="copyright-content">
                <p>Copyright © 2020 <a rel="nofollow" href="https://www.elitech.education" target="_blank" rel="noopener noreferrer">ELITECH</a> All Right Reserved</p>
              </div>
            </div>
          </div>
        </div>
      </div>   
    </footer> 
    <!-- Footer Section End -->

    <!-- Go to Top Link -->
    <a href="#" class="back-to-top">
    	<i class="lni-arrow-up"></i>
    </a>
    
    <!-- Preloader -->
    <div id="preloader">
      <div class="loader" id="loader-1"></div>
    </div>
    <!-- End Preloader -->
    
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="assets/js/jquery-min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/wow.js"></script>
    <script src="assets/js/jquery.nav.js"></script>
    <script src="assets/js/scrolling-nav.js"></script>
    <script src="assets/js/jquery.easing.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>      
    <script src="assets/js/waypoints.min.js"></script>   
    <script src="assets/js/main.js"></script>
      
  </body>
</html>
